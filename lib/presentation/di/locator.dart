import 'package:get_it/get_it.dart';
import 'package:graphql/client.dart';

import '../../common/network/connectivity_manager.dart';
import '../../data/mapper/character_response_entity_mapper.dart';
import '../../data/network/api_provider/api_provider.dart';
import '../../data/network/api_provider/network_constants.dart';
import '../../data/network/data_source/remote_data_source.dart';
import '../../data/repositories/character_repository_impl.dart';
import '../../domain/repositories/character_repository.dart';
import '../../domain/usecases/get_character_usecase.dart';
import '../providers/character_view_modal.dart';


final getIt = GetIt.instance;

_provideGraphQlClient() {
  _provideGraphQlClient();

  getIt.registerSingleton<CharacterResponseEntityMapper>(CharacterResponseEntityMapper());

  getIt.registerSingleton<ConnectivityManager>(ConnectivityManagerImpl());

  getIt.registerSingleton<ApiProvider>(GraphQLApiProvider(graphQLClient: getIt()));

  getIt.registerSingleton<RemoteDataSource>(RemoteDataSourceImpl(apiProvider: getIt<ApiProvider>()));

  getIt.registerSingleton<CharacterRepository>(CharacterRepositoryImpl(
    remoteDataSource: getIt<RemoteDataSource>(),
    connectivityManager: getIt<ConnectivityManager>(),
    characterResponseEntityMapper: getIt<CharacterResponseEntityMapper>(),
  ));

  getIt.registerSingleton<GetCharacterUseCase>(GetCharacterUseCase(repository: getIt<CharacterRepository>()));

  getIt.registerFactory(() => CharacterViewModel(getCharacterUseCase: getIt<GetCharacterUseCase>()));
}
  void initialiseDI() {
    _provideGraphQlClient();
  final _httpLink = HttpLink(BASE_URL);

  final GraphQLClient _client = GraphQLClient(
    cache: GraphQLCache(),
    link: _httpLink,
  );

  getIt.registerSingleton<GraphQLClient>(_client);
}