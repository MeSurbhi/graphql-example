import 'package:clean_arch_riverpod/domain/entities/character.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../data/network/api_provider/result.dart';
import 'character_view_modal.dart';



final characterViewModelProvider = ChangeNotifierProvider<CharacterViewModel>(
      (ref) => CharacterViewModel(getCharacterUseCase: ),
);
