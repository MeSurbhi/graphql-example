import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../data/network/api_provider/result.dart';
import '../../domain/entities/character.dart';
import '../../domain/usecases/get_character_usecase.dart';

class CharacterViewModel extends ChangeNotifier {
  final GetCharacterUseCase getCharacterUseCase;

  CharacterViewModel({required this.getCharacterUseCase});

  AsyncValue<Result<Character>> _characterValue = const AsyncValue.loading();

  AsyncValue<Result<Character>> get nowPlayingMovies => _characterValue;
  final _characters = <Character>[];

  searchCharacter(String name) async {
    _characterValue = const AsyncValue.loading();
    final Result<Character> result = await getCharacterUseCase(
        GetCharacterParam(name: name));
    if (result is Success) _characters.add((result as Success).data);
    _characterValue = AsyncValue.data(result);
  }

  getCachedList() => _characters;
}