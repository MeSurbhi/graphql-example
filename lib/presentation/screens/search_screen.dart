import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../data/network/api_provider/result.dart';
import '../../domain/entities/character.dart';
import '../providers/character_view_modal.dart';
import '../providers/view_model_provider.dart';

class SearchScreen extends ConsumerWidget {

  final TextEditingController _controller = TextEditingController();
  late CharacterViewModel characterViewModel;

  SearchScreen({super.key});
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: _buildMainBody(context, ref),
    );
  }

  _buildMainBody(context, ref) {
    return Column(
      children: [
        _buildSearchForm(),
        _buildCharacterCard(ref),
      ],
    );
  }

  _buildSearchForm() {
    return Card(
      margin: const EdgeInsets.all(16),
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        child: TextFormField(
          onFieldSubmitted: (query) async {
            _controller.clear();
            characterViewModel.searchCharacter(query);
          },
          controller: _controller,
          decoration: const InputDecoration(
              border: InputBorder.none, hintText: 'Search Anime Characters'),
        ),
      ),
    );
  }

  Widget _buildCharacterCard(ref) {
    AsyncValue<Result<Character>> characters = ref.watch(characterViewModelProvider);

    return Consumer(builder: (context, ref, _) {
      return characters.when(data: (data) {
        return _createMyListView(context, data);
      }, loading: () {
        return CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                Theme.of(context).colorScheme.secondary));
      }, error: (e, s) {
        return const SizedBox();
      });
    });
  }

  _createMyListView(context, Result<Character> character) {
    final size = MediaQuery.of(context).size;

    return Container(
        height: size.height * 0.2,
        child:  ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: characterViewModel.getCachedList().length,
                itemBuilder: (BuildContext context, int index) {
                  final item = characterViewModel.getCachedList()[index];
                  return _CardItem(character: item, isSelected: false);
                }),

        );
  }
}

class _CardItem extends StatelessWidget {
  final Character character;
  final bool isSelected;

  _CardItem({required this.character, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Card(
      elevation: 4,
      child: Container(
        width: size.height * 0.2,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.network(character.image, fit: BoxFit.cover),
            Column(
              children: [
                const Expanded(child: SizedBox()),
                Container(
                  color: Colors.red,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Center(
                      child: Text(
                        character.name,
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
