import 'package:clean_arch_riverpod/presentation/screens/search_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) {
    return  ProviderScope(child:MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Issues',
      theme: ThemeData(
        primaryColor: Colors.blueGrey,
        primaryColorDark: Colors.blueGrey[700],
        primaryColorLight: Colors.blueGrey[300],
        backgroundColor: Colors.white,
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.deepOrange[700]),
      ),
      home: const Scaffold(
        body: SearchScreen(),
      ),
    ),);
  }
}