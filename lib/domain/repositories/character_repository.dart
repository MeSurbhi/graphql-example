import 'package:clean_arch_riverpod/domain/entities/character.dart';

import '../../data/network/api_provider/result.dart';

abstract class CharacterRepository {
  Future<Result<Character>> fetchCharacters(String name);
}