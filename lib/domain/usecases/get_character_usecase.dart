import 'package:clean_arch_riverpod/domain/usecases/usecase.dart';

import '../../data/network/api_provider/result.dart';
import '../entities/character.dart';
import '../repositories/character_repository.dart';

class GetCharacterUseCase extends UseCase<GetCharacterParam, Character> {
  final CharacterRepository repository;

  GetCharacterUseCase({required this.repository});

  @override
  Future<Result<Character>> call(GetCharacterParam param) async {
    return await repository.fetchCharacters(param.name);
  }
}

class GetCharacterParam {
  final String name;

  GetCharacterParam({required this.name});
}
