
import '../../data/network/api_provider/result.dart';

abstract class UseCase<Param, T> {
  Future<Result<T>> call(Param param);
}