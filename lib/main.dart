import 'package:clean_arch_riverpod/presentation/application.dart';
import 'package:clean_arch_riverpod/presentation/di/locator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

void main() => run();

Future run() async {
  WidgetsFlutterBinding.ensureInitialized();

  initialiseDI();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(Application());
  });
}
