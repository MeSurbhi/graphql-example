import 'package:clean_arch_riverpod/data/network/api_provider/result.dart';

import 'package:clean_arch_riverpod/domain/entities/character.dart';

import '../../common/network/connectivity_manager.dart';
import '../../domain/repositories/character_repository.dart';
import '../mapper/character_response_entity_mapper.dart';
import '../network/data_source/remote_data_source.dart';

class CharacterRepositoryImpl implements CharacterRepository {
  final RemoteDataSource remoteDataSource;
  final ConnectivityManager connectivityManager;
  final CharacterResponseEntityMapper characterResponseEntityMapper;

  CharacterRepositoryImpl(
      {required this.remoteDataSource,
      required this.connectivityManager,
      required this.characterResponseEntityMapper});

  @override
  Future<Result<Character>> fetchCharacters(String name) {
    return _checkNetworkAvailability<Result<Character>>(
        doCacheCall: () => _getRemoteCharacter(name),
        doNetworkCall: () => _getRemoteCharacter(name));
  }

  Future<T> _checkNetworkAvailability<T>(
      {required Future<T> doNetworkCall(), required Future<T> doCacheCall()}) {
    return connectivityManager.isConnected.then((isNetworkAvailable) {
      return isNetworkAvailable ? doNetworkCall() : doCacheCall();
    }, onError: (e) {
      Error(error: Exception());
    });
  }

  Future<Result<Character>> _getRemoteCharacter(String name) async {
    try {
      final characterResponse = await remoteDataSource.fetchCharacters(name);
      return Success(
          data: characterResponseEntityMapper
              .mapFromOriginalObject(characterResponse));
    } catch (exception) {
      return Error(error: Exception());
    }
  }
}
