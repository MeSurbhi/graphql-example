import 'package:graphql/client.dart';

import '../../models/character_response.dart';
import 'exceptions.dart';
import 'graphql_query.dart';

abstract class ApiProvider {
  Future<CharacterResponse> fetchCharacters(String name);
}

class GraphQLApiProvider extends ApiProvider {
  final GraphQLClient graphQLClient;

  GraphQLApiProvider({required this.graphQLClient});

  @override
  Future<CharacterResponse> fetchCharacters(String name) async {
    final QueryOptions queryOptions = QueryOptions(
      document: gql(searchCharacter),
      variables: {'character': name},
    );
    final QueryResult result = await graphQLClient.query(queryOptions);
    if (result.hasException) {
      throw ServerFailureException();
    }
    if (result.data != null) {
      return CharacterResponse.fromJson(result.data!);
    }
    throw EmptyResponseException();
  }
}
