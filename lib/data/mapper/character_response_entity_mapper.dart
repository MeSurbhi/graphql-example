
import '../../common/mapper/entity_model_mapper.dart';
import '../../domain/entities/character.dart';
import '../models/character_response.dart';

class CharacterResponseEntityMapper extends ObjectMapper<CharacterResponse, Character> {
@override
  Character mapFromOriginalObject(CharacterResponse originalObject) {
    return Character(
      name: originalObject.name,
      image: originalObject.image,
    );
  }
}