
import '../models/character_response.dart';
import '../network/api_provider/api_provider.dart';
import '../network/api_provider/exceptions.dart';

abstract class RemoteDataSource {
  Future<CharacterResponse> fetchCharacters(String name);
}

class RemoteDataSourceImpl extends RemoteDataSource {
  final ApiProvider apiProvider;

  RemoteDataSourceImpl({required this.apiProvider});

  @override
  Future<CharacterResponse> fetchCharacters(String name) {
    try {
      return apiProvider.fetchCharacters(name);
    } catch (e) {
      throw ServerFailureException();
    }
  }
}
